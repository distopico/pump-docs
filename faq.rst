Frequently Asked Questions
==========================

Seldomly asked questions. By real users. Not those clever developers.
Here's also a `basic user guide <userguide.html>`_.


----


How do I follow a user on a remote instance?
--------------------------------------------

OK - so you now have your new, shiny identi.ca account migrated to the wonderful Pump platform, or a new Pump account on any other server. In the former case, obviously all your existing Friends/Followers subscriptions from identi.ca have been seamlessly migrated, but sooner or later, you will make a new friend. Honest, you will. No, you will. And in the latter case, you'll probably want to start following someone, right?

If your new mate is also on your same server, then it's easy enough. Very similar to how it used to be on identi.ca in StatusNet times, in fact, just click 'Follow'. The 'Follow' button should magically transform to 'Following'.

Let's imagine you want to follow a user on a remote Pump instance. For example, you see an interesting note from 'david@fmrl.me'. Ensure you are logged in to your Pump account. Then visit David's Pump page at https://fmrl.me/david

Click 'Follow' as before but instead of entering your login credentials on the remote instance, click 'Account on another server ?' and type in your Webfinger identifier, such as 'harry@identi.ca'.

Click 'Authorize' when asked 'Do you want to authorize fmrl.me to access your account?' and then click 'Follow' when the user's home page is displayed.

Again, this isn't too far removed for the process for subscribing to a remote StatusNet user, but the GUI may surprise a few people at first.

As a start, you might want to check the `Users by language <https://github.com/e14n/pump.io/wiki/Users-by-language>`_ list.



Why isn't my post available on the Internet?
--------------------------------------------

If you have spent hours lovingly crafting a note on Pump and proudly posted the link to all your friends and family, there's nothing worse than people telling you 'That link you sent doesn't work'.

Pump has strict controls on privacy and publishing. By default, a note will only be posted (and visible) to your 'Followers'. If you want a note to be visible to everyone on the Internet, ensure you include 'Public' in the 'To:' or 'CC:' list.

There are enhancement requests (`#364 <https://github.com/e14n/pump.io/issues/364>`_) for Pump to set defaults for your preferred distribution list or, alternatively, to remember (`#431 <https://github.com/e14n/pump.io/issues/431>`_) the settings used on your last post.

Many of the other `clients <clients.html>`_ already have the option to always post to Public.



Can I include HTML markup in the WYSIWYG post editor?
-----------------------------------------------------

In the web interface, no. However, some other `clients <clients.html>`_ support Markdown.



Is there a public timeline?
---------------------------

Well, yes and no. The web interface does not show a public timeline. Pump.io servers post the public notes from their users to `OFireHose <https://ofirehose.com>`_ and it is possible to obtain `a feed from there, in ActivityStreams format (JSON) <https://ofirehose.com/doc/subscribe>`_. So `clients <clients.html>`_ may present a public timeline using that feed. At this moment, Puma (client for Android) and Pumpa (desktop client) show the Firehose timeline. Web interfaces to the public timeline are in the 'Hubub' deployments (see `Clients <clients.html>`_): http://hubub.e43.eu/ , http://hubub.jpope.org/ and http://ppump.redaustral.tk/rss.php (RSS feed).
For more information, you can visit `OFirehose.com <https://ofirehose.com/>`_, or track `issue #656 <https://github.com/e14n/pump.io/issues/656>`_.



Are there groups like in StatusNet?
-----------------------------------

Not yet. There is an open issue about group support (`#299 <https://github.com/e14n/pump.io/issues/299>`_), you can track it to know when/how it will be implemented (you can help to make it happen, too!).

Support for groups is partially implemented at the moment. Though the web interface doesn't have anything for it, it's possible to create, join and delete groups. The Dianara client has experimental support for this, but posting to a group requires manually keeping track of group ID's, and the comments to notes posted to the groups are not well distributed to all members. This still needs some work before groups are usable.



Why can't I see comments in threaded view?
------------------------------------------

By now, it's technically possible to reply to a comment, and it is displayed in the minor inbox feed (the "Meanwhile..." column), and the original poster and commenter receive a notification. However, it's not clear if it is the intended behavior, or only comments to the original post should be allowed. You can track `issue #497 <https://github.com/e14n/pump.io/issues/497>`_ and participate in the discussion about this topic.
https://github.com/e14n/pump.io/issues/497



What is the visibility of comments?
-----------------------------------

If I comment on someone else's post, how public is my comment?

If you comment on somebody's post, it depends on the original posts audience. If it's a public post, your comment will be public. If the post was to a limited audience, only the original author will be able to see your comment. Some of this may change a little in the future...



What is the visibility of a shared post?
----------------------------------------

If I share someone else's post, how public is my share?  Is it only shared to my followers?

If you share someone else's post, again it depends on the original author's audience. If the original post was public, the reshare will be public. If the original post was to a limited audience, the reshare is supposed to only be visible to the original authors audience, but, currently, it goes out to your followers. Honestly, if the original author posts something to a limited audience, maybe it shouldn't be able to be reshared at all. 



Why can't I find somebody when fulfilling the To: or CC: boxes?
----------------------------------------------------------------

You can send/CC to the people you follow.

If you are following somebody and you cannot find him/her when you type the name in the To: or CC: box, maybe you are experiencing this issue:
https://github.com/e14n/pump.io/issues/805

Some of the `clients <clients.html>`_ will also allow you to address a post to certain people just by typing @ and selecting the user from a pop-up list. It's just another way to automatically fill the To: or CC: fields.



How can I set or change my e-mail address?
------------------------------------------

While the web interface doesn't provide a way to do this yet, the `Dianara client <clients.html>`_ has an option for this, since version 1.3.1.



Why can't I post a comment to a certain post?
---------------------------------------------

You're probably suffering from `this issue, #1027 <https://github.com/e14n/pump.io/issues/1027>`_.

This basically happens when you are on server A, and post to a note from a user on server B that no one (including you) on server A is following. You might be seeing this because someone you follow shared it, but since nobody on your server follows the author of the post, the post was never really "delivered" to your server, and that results in the "no original post" error.

This error is especially unclear in the web interface. There's not really a workaround for this yet, but if you follow the author of that post, you'll be able to comment on their **future** posts.

