Installation channels
=====================

This page documents where you can obtain pump.io and the different levels of support we offer for each distribution channel.

Anything not listed on this page should be considered completely unsupported by the pump.io project, and you should carefully evaluate the risk before using any such resource.

+--------------------------------------------+------------------+-----------------+------------------------------+---------------------------------------------------------------------+
| Channel                                    | Security support | Betas available | Coordinated release schedule | Recommended for                                                     |
+============================================+==================+=================+==============================+=====================================================================+
| `npm registry <generic-npm.html>`_         | Yes              | Yes             | Direct                       | General use                                                         |
+--------------------------------------------+------------------+-----------------+------------------------------+---------------------------------------------------------------------+
| `GitHub clone <generic-source.html>`_      | Yes              | Yes             | Direct                       | Installations which need to patch the source code                   |
+--------------------------------------------+------------------+-----------------+------------------------------+---------------------------------------------------------------------+
| `Docker image <about-docker-images.html>`_ | Yes              | Yes             | Direct                       | Administrators with existing Docker infrastructure they want to use |
+--------------------------------------------+------------------+-----------------+------------------------------+---------------------------------------------------------------------+

- **Security support** means that the pump.io project guarantees it will support installations using this channel with security updates.
- **Betas available** means that you can acquire pump.io betas as well as stable releases from this channel.
- **Coordinated release schedule** indicates whether this channel will have the latest release as soon as pump.io upstream cuts a release; "direct" indicates that the pump.io project is directly responsible for releases through this channel, and "coordinated" means that pump.io will coordinate schedules with this channel to ensure that updates are timely.
- **Recommended for** describes which scenarios you should use this installation method for.
