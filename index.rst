=====
Start
=====

.. toctree::
   :maxdepth: 2
   :caption: User Documentation

   userguide
   webui
   faq
   clients
   builtin-cli

.. toctree::
   :maxdepth: 2
   :caption: Sysadmin Documentation

   sysadmins
   sysadmins-faq

.. toctree::
   :maxdepth: 2
   :caption: Developer Documentation

   developers


For now, most of the documentation is at `the old wiki <https://github.com/e14n/pump.io/wiki>`_.

Check out the `community information <https://github.com/e14n/pump.io/wiki/Community>`_.
