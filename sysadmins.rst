For system administrators
=========================

Installation instructions
-------------------------

npm is the recommended installation method if there isn't
documentation for your specific distribution listed below. The pump.io
project `strongly` recommends that users avoid source-based installs.

.. toctree::
   :maxdepth: 1

   installation/prerequisites
   installation/installation-channels
   installation/generic-npm
   installation/generic-source
   installation/about-docker-images

Configuration details
---------------------

.. toctree::
   :maxdepth: 2

   configuration/reference
   configuration/via-cli-flags
   configuration/via-environment-variables
   configuration/via-json-config-files

..

.. toctree::
   :maxdepth: 2

   configuration/running-the-daemon
   configuration/set-node-environment

..

.. toctree::
   :maxdepth: 2

   configuration/certbot
   configuration/cacert

..

.. toctree::
   :maxdepth: 2

   configuration/web-server-proxy

Upgrade instructions
--------------------

.. toctree::
   :maxdepth: 2

   1.x to 2.x <upgrades/1.x-to-2.x>
   2.x to 3.x <upgrades/2.x-to-3.x>
   3.x to 4.x <upgrades/3.x-to-4.x>
   4.x to 5.x <upgrades/4.x-to-5.x>

Routine maintenance
-------------------

.. toctree::
   :maxdepth: 2

   administration/viewing-logs
   administration/upstream-systemd-unit
   administration/zero-downtime-restarts
   administration/migrating-hosts

See also the `Sysadmin FAQ <sysadmins-faq.html>`_.
