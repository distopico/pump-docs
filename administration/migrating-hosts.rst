Migrating hosts
===============

If you ever want to migrate to another server, you need to copy the
contents of your Databank backend as well as the ``datadir``. Before
you perform this procedure, make sure to shut down your pump.io
server.

For example, if you're using ``databank-mongodb`` you could do
something like this:

::

    $ systemctl stop pump.io@mongodb
    $ mongodump
    $ rsync -var dump new.server.com:
    $ rsync /var/local/pumpio/ new.server.com:

    $ ssh new.server.com
    $ mongorestore
    $ mv pumpio /var/local/pumpio/

This example assumes that you're moving to ``new.server.com`` and that
your ``datadir`` is set to ``/var/local/pumpio/``.
