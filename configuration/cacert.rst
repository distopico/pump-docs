Self-signed certs and CACert non-support in pump.io
===================================================

Over the years there have been some requests to support self-signed
certificates or CACert certificates in pump.io. Self-signed
certificates and certificates signed by less-trusted providers like
CACert appear to work when you're installing, but later your server
will have trouble communicating with other pump.io servers.

This trouble occurs because while `you` may have CACert or your
self-signed certificatein your operating system's trust store, other
pump.io servers will not. When your server tries to deliver posts to
them, or they to you, it won't work because the other servers won't
trust your server and distribution will fail.

Support for these certs will never appear in pump.io core. Not only
would it be a burden to maintain, it would compromise the security of
the overall network - and for no good reason since reasonable
alternatives exist.

If you are looking for a free TLS certificate, please consider Let's
Encrypt. `Documentation is available <certbot.html>`_ on how to use
Certbot to automatically get Let's Encrypt certificates for pump.io.
