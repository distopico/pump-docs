Clients and Services
====================

These are some client applications and services that support Pump.io.

See also: `Libraries <https://github.com/e14n/pump.io/wiki/Libraries>`_


Web
---

* `ih8.it <https://ih8.it>`_ - Hate button for the web.
* `hip2.it <https://hip2.it>`_ - Like button.
* `lurve.jpope.org <https://lurve.jpope.org>`_ - Love button.
* `pump2status.net <https://pump2status.net>`_ - Find your StatusNet/GNUsocial friends on the pump network.
* `pump2tweet.com <https://pump2tweet.com>`_ - Send activities from the pump network to Twitter.
* `OpenFarmGame <https://openfarmgame.com>`_ - A farming game for the pump network.
* `PumpLive <http://pumplive.com>`_ - Stats server.
* `OFirehose <https://ofirehose.com>`_ - The pump network firehose.
* `brdcst.it <http://brdcst.it>`_ - Broadcast your blog, µblog, etc feeds to your social networks, including Pump.io.
* `pump2rss.com <https://pump2rss.com>`_ - Generates an RSS (Atom) feed of the activity stream.
* `rss.io.jpope.org <https://rss.io.jpope.org>`_ - Alternate for pump2rss.com.
* `pumpiostatus.website <http://pumpiostatus.website>`_ - Check the status and uptime of all registered Pump servers.
* `Granada <https://gitlab.com/sotitrox/granada>`_- Website, bookmarklet and button for publishing content from other places in Pump.io ("Share in Pump.io" thingy). Website in Spanish. Other deployments: http://granada.mamalibre.com.ar.
* `hubub.e43.eu <http://hubub.e43.eu>`_ - View the Firehose aka what's happening in Pump.io, in real time. Source code `here <https://github.com/oshepherd/hubub>`_. Other deployments: https://hubub.polari.us.
* `PumpBridge <https://pumpbridge.me>`_ - Connects Pump.io to facebook and googleplus. Source code `here <https://wald.intevation.org/projects/pumpbridge/>`_.
* `PPump <ttps://gitlab.com/sotitrox/ppump>`_ - Ppump: RSS Feed of the Firehose and public user directory. Live instances at https://www.inventati.org/ppump and https://pump.mamalibre.com.ar.



Desktop
-------

* `Dianara <https://jancoding.wordpress.com/dianara>`_ - A Qt desktop app.
    * `MSwindows builds <https://www.luisgf.es/pump/>`_ of Dianara. Website in Spanish, for now.
* `Pumpa <http://saz.im/software/pumpa.html>`_ - Another Qt client under development.
    * `How to <https://github.com/e14n/pump.io/wiki/HowTo-for-building-Pumpa-on-OS-X>`_ for building Pumpa on OS X.
* `Choqok <http://choqok.gnufolks.org/>`_ - KDE micro-blogging client.

* `spigot <https://pypi.python.org/pypi/spigot/>`_ - Console client for rate-limited (RSS) feed aggregation. Implemented in Python via PyPump.
* `PumpTweet <https://github.com/dper/PumpTweet>`_ - Find notes from your Pump account, shorten them, make a URL to the original note, and post the short version as a tweet on Twitter. It can also be used for GNU Social (StatusNet).
* `PumpMigrate <https://github.com/kabniel/pumpmigrate>`_ - Move or sync contacts between Pump.io accounts.
* `p <https://github.com/xray7224/p>`_ - A Pump.io version of the command line utility 't'.
* `NavierStokes <http://polari.us/dokuwiki/doku.php?id=navierstokes>`_ - Allows you to bridge between social network accounts. ALPHA release.
* `Pumpio-el <http://www.emacswiki.org/emacs/?action=browse;oldid=Pumpio-el;id=PumpioEl>`_ - Pump.io client for Emacs.
* `GPump <https://launchpad.net/gpump>`_ - A GTK+ Pump.io client in the concept stages.
* `Manivela <https://gitlab.com/sotitrox/manivela>`_ - Command line client written in PHP. Documentation in Spanish.



Android
-------

* `Impeller <http://impeller.e43.eu/>`_ - ICS (4.0) or above - `Google Play <https://play.google.com/store/apps/details?id=eu.e43.impeller>`_ / `Download APK <https://dl.dropboxusercontent.com/u/64151759/Impeller.apk>`_
* `Puma <https://gitorious.org/puma-droid>`_ - `Download APK <http://macno.org/puma/>`_
* `PumpFM <https://gitorious.org/pumpfm>`_ - A simple app that scrobbles the music listened on your Android phone to a Pump.io instance (`Link to binary .apk <https://static.jpope.org/files/Pumpfm.apk>`_)
* `AndStatus <https://github.com/andstatus/andstatus/wiki>`_ - Multiple Pump.io, GNU Social and Twitter accounts. Can work offline.



iOS
---

* `Social Monkeys <http://www.roguemonkey.in/>`_ - An intuitive  iOS client to manage your Pump.io social activity stream.
